var simpleStatistics = require('simple-statistics');

module.exports = function (RED) {
    function AggregatorNode(config) {
        RED.nodes.createNode(this, config);
        var node = this;

        var intervalCount = config.intervalCount;
        var intervalUnits = config.intervalUnits;
        var intervalType = config.intervalPeriod;
        var factor = 1;
        var d = new Date();
        d.setHours(0,0,0,0);
        node.midnight = d.getTime();
        node.intervals = [];
        node.intervalsFromInput = false;
        node.calcintervalTimeout = function(timeout) {
            return timeout - (new Date() % timeout);
        };

        node.aggregate = function (list) {
            var output = null;

            switch (config.aggregationType) {
                case "count":
                    if(list[0].hasOwnProperty('count')){
                        output = list[0].count + list.length - 1;
                    }else {
                        output = list.length;
                    }                    
                    list = [{'count': output}];
                    break;
                    // functions with 0 parameter and data
                case "min":
                case "minSorted":
                case "max":
                case "maxSorted":
                case "sum":
                case "sumSimple":
                case "product":
                    output = simpleStatistics[config.aggregationType](list);
                    list = [output];
                    break;
                case "mean":
                    var length = list.length;
                    if(list[0].hasOwnProperty('count')){
                        var prevMean = list[0];
                        list[0] = list[0].sum;
                        length = prevMean.count + length - 1;
                    }
                    list = [{sum: simpleStatistics["sum"](list), count: length}]
                    output = list[0].sum / list[0].count;
                    break;
                case "mode":
                case "modeSorted":
                case "median":
                case "medianSorted":
                case "harmonicMean":
                case "geometricMean":
                case "rootMeanSquare":
                case "sampleSkewness":
                case "variance":
                case "sampleVariance":
                case "standardDeviation":
                case "sampleStandardDeviation":
                case "medianAbsoluteDeviation":
                case "interquartileRange":
                case "shuffle":
                case "shuffleInPlace":
                case "uniqueCountSorted":
                case "permutationsHeap":
                    output = simpleStatistics[config.aggregationType](list);
                    break;
                case "uniqueCount":
                    output = simpleStatistics['uniqueCountSorted'](list);
                    break;

                    // functions with 1 parameter and data
                case "quantile":
                case "quantileSorted":
                case "sumNthPowerDeviations":
                case "sampleCorrelation":
                case "sampleCovariance":
                case "sample":
                case "tTest":
                case "ckmeans":
                case "equalIntervalBreaks":
                case "chunk":
                case "combinations":
                case "combinationsReplacement":
                    value = parseFloat(config.funcParameter);
                    if (isNaN(value)) {
                        node.warn("Non-numeric parameter to function " + config.aggregationType + " received: " + config.funcParameter);
                    } else {
                        output = simpleStatistics[config.aggregationType](list, value);
                    }
                    break;
                case "rSquared":
                    if (!typeof config.funcParameter === "function") {
                        node.warn("Non-function parameter to function " + config.aggregationType + " received: " + config.funcParameter);
                    } else {
                        output = simpleStatistics[config.aggregationType](list, config.funcParameter);
                    }
                    break;
                    // others
                default:
                    node.warn("Undefined function " + config.aggregationType);
                    output = null;
                    break;
            }
            return output;
        };
        node.aggregateInterval = function (intervalObj, clear, singleTopic) {
            var results = [];
            function addToResults(t){
                if (intervalObj.values[t].length > 0) {
                    results.push({ 
                        'topic': t,
                        'interval': intervalObj.interval, 
                        'intervalStart': intervalObj.intervalStart,
                        'intervalEnd': intervalObj.intervalEnd,
                        'payload': node.aggregate(intervalObj.values[t]),
                        'aggregationType': config.aggregationType,
                    });
                }
            }
            if(singleTopic){
                if (intervalObj.values.hasOwnProperty(singleTopic)) {
                    addToResults(singleTopic)
                }
            }else{
                for (var topic in intervalObj.values) {
                    var t = "";
                    if (intervalObj.values.hasOwnProperty(topic)) {
                        t = topic;
                    } else {
                        t = config.topic;
                    }
                    addToResults(t);
                }
            }
            for (var i = 0; i < results.length; i++) {
                node.send(results[i]);
            }
            if(clear){
                intervalObj.values = {};
            }
        };

        node.clearTimersAtIndex = function(index) {
            if (node.intervals[index].timer) {
                clearInterval(node.intervals[index].timer);
            }
            clearTimeout(node.intervals[index].primaryTimeout);
        };

        node.calcIntervalTimeoutWithUnits = function(intervals, units){
            switch (units) {
                case "s":
                default :
                    factor = 1000;
                    break;
                case "m":
                    factor = 1000 * 60;
                    break;
                case "h":
                    factor = 1000 * 60 * 60;
                    break;
                case "d":
                    factor = 1000 * 60 * 60 * 24;
                    break;
            }
            return factor * intervals;
        };

        node.calcIntervalTimeoutArray = function(intervals, units){
            var intervalTimeout =[];
            for(var i = 0; i < intervals.length; i++){
                if(intervals[i].hasOwnProperty('length') && intervals[i].hasOwnProperty('default')){
                    intervalTimeout.push({'length': node.calcIntervalTimeoutWithUnits(intervals[i].length, units), 'default': intervals[i].default});
                }else{
                    intervalTimeout.push(node.calcIntervalTimeoutWithUnits(intervals[i], units));
                }
            }
            return intervalTimeout;
        };

        node.removeIntervals= function(topic, intervals) {
            if(!intervals){
                intervals = [];
                for (var i = 0; i < node.intervals.length; i++) {
                    var intervalObj = node.intervals[i];
                    var topicIndex = intervalObj.topics.indexOf(topic);
                    if(topicIndex!= -1){
                        intervals.push(intervalObj.interval);
                    }
                }    
            }
            for (var i = 0; i < intervals.length; i++) {
                node.removeInterval(topic, intervals[i]);
            }
        };
        node.removeInterval = function(topic, interval) {
            for (var i = 0; i < node.intervals.length; i++) {
                var intervalObj = node.intervals[i];
                if (intervalObj.interval == interval) {
                    var topicIndex = intervalObj.topics.indexOf(topic);
                    if(topicIndex!= -1){
                        // remove this topic from interval
                        intervalObj.topics.splice(topicIndex, 1);
                        if(intervalObj.values.hasOwnProperty(topic)){
                            delete intervalObj.values[topic];
                        }
                    }
                    if(intervalObj.topics.length == 0){
                        // no more topics to calculate
                        node.clearTimersAtIndex(i);
                        node.intervals.splice(i, 1);
                        return null;
                    }
                    return intervalObj;
                }
            }
            return null;
        };

        node.addIntervalsWithUnits = function(topic, intervals, units){
            var unitIntervals = node.calcIntervalTimeoutArray(intervals, units);
            for (var i = 0; i < unitIntervals.length; i++) {
                node.addInterval(topic, unitIntervals[i].hasOwnProperty('length')? unitIntervals[i].length: unitIntervals[i], unitIntervals[i].hasOwnProperty('default')?unitIntervals[i].default:undefined);
            }
        };

        node.addInterval = function(topic, interval, defaultValue) {
            var intervalObj = node.removeInterval(topic, interval);
            if(intervalObj === null){
                var startupTimeout = node.calcintervalTimeout(interval);
                var now = new Date().getTime();
                var vals = {};
                if(defaultValue){
                    vals[topic] = [defaultValue];
                }
                var intervalObj = { 'topics': [topic], 'interval': interval, 'intervalStart': now - (interval - startupTimeout),'intervalEnd': now + startupTimeout, 'values': vals, 'timer': null, 'primaryTimeout': null,  }
                intervalObj.primaryTimeout = setTimeout(function() {
                    node.timeoutFn(intervalObj);
                }, startupTimeout);
                node.intervals.push(intervalObj);
                node.intervals.sort(function(a,b){return a.interval - b.interval;});
            }else {
                intervalObj.topics.push(topic);
                if(defaultValue){
                    intervalObj.values[topic] = [defaultValue];
                }
            }            
        };

        node.timeoutFn = function (intervalObj) {
            intervalObj.timer = setInterval(function() {
                node.aggregateInterval(intervalObj, true);// aggregates and clears data 
                var now = new Date().getTime();
                now = now - (now % intervalObj.interval);
                intervalObj.intervalStart = now - intervalObj.interval;
                intervalObj.intervalEnd = now;
            }, intervalObj.interval);
            if (config.submitIncompleteInterval) {
                node.aggregateInterval(intervalObj, true); // aggregates and clears data 
            } else {
                intervalObj.values = {}; // throw away data 
            }
        };

        node.on('input', function (msg) {
            try {
                var lowerTopic = msg.topic.toString().toLowerCase();
                if (node.intervalsFromInput && msg.hasOwnProperty('interval')) {
                    if (lowerTopic === "") {
                        lowerTopic = config.topic;
                    }
                    var intervals = [];
                    function pushInterval(interval){
                        if(Number.isInteger(interval)){
                        // interval without default value
                            intervals.push({length: interval, default: null});
                        } else if(interval.hasOwnProperty('length') && interval.hasOwnProperty('default')){
                            //interval with default value
                            intervals.push({length: interval.length, default: interval.default});
                        }
                    }
                    if (Array.isArray(msg.interval)){
                        //multiple intervals
                        for(var i = 0; i < msg.interval.length; i++){
                            pushInterval(msg.interval[i]);
                        }

                    } else {
                        pushInterval(msg.interval);
                    }
                    
                    var intervalUnits = msg.hasOwnProperty('intervalUnits') ? msg.intervalUnits : 's';
                    //remove old intervals
                    node.removeIntervals(lowerTopic);
                    //add new aggregation intervals for the topic
                    node.addIntervalsWithUnits(lowerTopic, intervals, intervalUnits);
                    return;
                }
                if (msg.payload !== null && msg.payload !== '') {
                    if (lowerTopic === "") {
                        lowerTopic = config.topic;
                    }
                    for (var i = 0; i < node.intervals.length; i++) {
                        var intervalObj = node.intervals[i];
                        if (!intervalObj.values[lowerTopic]) {
                            if(node.intervalsFromInput&& intervalObj.topics.indexOf(lowerTopic) == -1){
                                continue;
                            }
                            intervalObj.values[lowerTopic] = [];
                        }
                        intervalObj.values[lowerTopic].push(parseFloat(msg.payload, 10));
                        if (config.submitIncompleteInterval) {
                            node.aggregateInterval(intervalObj, false, lowerTopic); // aggregates and does not clear data 
                        }
                    }
                }
            } catch (err) {
                node.error(err.message);
            }
        });

        node.on('close', function () {
            for (var i = 0; i < node.intervals.length; i++) {
                node.clearTimersAtIndex(i);
            }
        });

        switch (intervalType) {
            case "single":
                node.intervalsFromInput = false;
                break;
            case "from input":
            default:
                node.intervalsFromInput = true;
                break;
        }
        if (!node.intervalsFromInput) {
            node.addIntervalsWithUnits(config.topic, [intervalCount], intervalUnits);
        }               
    }

    RED.nodes.registerType("aggregator", AggregatorNode);
};